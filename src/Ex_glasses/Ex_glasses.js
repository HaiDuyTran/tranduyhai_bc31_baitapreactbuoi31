import React, { Component } from "react";
import { glassesArr } from "./data_glasses";
import GlassesDetail from "./GlassesDetail";
import GlassesList from "./GlassesList";
import "./Ex_glasses.css";

export default class Ex_glasses extends Component {
  state = {
    glassesArr: glassesArr,
    glassesDetail: {},
  };
  handleShowDetail = (sp) => {
    let cloneGlassesDetail = { ...this.state.glassesDetail };
    cloneGlassesDetail.url = `./glassesImage/v${sp.id}.png`;
    cloneGlassesDetail.name = `${sp.name}`;
    cloneGlassesDetail.desc = `${sp.desc}`;

    this.setState({
      glassesDetail: cloneGlassesDetail,
    });
  };
  render() {
    return (
      <div className="background"
        style={{
          backgroundImage: `url("./glassesImage/background.jpg")`,
          
        }}
      >
        <div className="container py-5 mx-auto">
          <h2 className="header">TryGlasses</h2>
          <GlassesDetail data={this.state.glassesDetail} />
          <GlassesList
            data={this.state.glassesArr}
            handleShow={this.handleShowDetail}
          />
        </div>
      </div>
    );
  }
}
