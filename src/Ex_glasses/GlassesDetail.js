import React, { Component } from "react";
import "./GlassesDetail.css";

export default class GlassesDetail extends Component {
  render() {
    let { name, desc, url } = this.props.data;
    return (
      <div
        className="glassesDetail"
        style={{
          backgroundImage: `url("./glassesImage/model.jpg")`,
          backgroundSize: "cover",
        }}
      >
        <div className="glasses">
          <img src={url}  alt="" />
        </div>
        <div className="detail">
          <h2>{name}</h2>
          <span>{desc}</span>
        </div>
      </div>
    );
  }
}
