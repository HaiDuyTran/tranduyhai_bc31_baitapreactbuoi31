import React, { Component } from "react";

export default class GlassesItem extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className="col-2">
        <img
          src={url}
          style={{ width: "8rem" }}
          alt=""
          onClick={() => {
            this.props.handleOnClick(this.props.data);
          }}
        />
      </div>
    );
  }
}
