import React, { Component } from "react";
import GlassesItem from "./GlassesItem";

export default class GlassesList extends Component {
  renderGlassesList = () => {
    return this.props.data.map((item) => {
      return <GlassesItem data={item} handleOnClick={this.props.handleShow} />;
    });
  };
  render() {
    return (
      <div>
        <div
          className="row mt-5 p-2"
          style={{ backgroundColor: "rgb(233, 205, 205,0.5)" }}
        >
          {this.renderGlassesList()}
        </div>
      </div>
    );
  }
}
